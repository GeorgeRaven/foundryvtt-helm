# FoundryVTT Kubernetes Helm-Chart

This has been moved to https://gitlab.com/GeorgeRaven/raven-helm-charts

This helm chart provides easy deployment of FoundryVTT on kubernetes clusters.
This chart uses https://hub.docker.com/r/felddy/foundryvtt as the base image.
This chart links this image to a secret of your own creation to enable it to download the server.


## Quick Start

```bash
helm repo add foundryvtt https://gitlab.com/api/v4/projects/53366175/packages/helm/stable
helm repo update foundryvtt
helm install foundryvtt foundryvtt/foundryvtt
```

Then create your secret in the same namespace as the helm chart (if you havent specified it will go into ``default``), by default the secret is called ``foundryvtt``:

```bash
kubectl create secret generic foundryvtt -n <NAMESPACE> --from-literal=foundryUsername=<YOUR_FOUNDRY_EMAIL> --from-literal=foundryPassword=<YOUR_FOUNDRY_PASSWORD> --from-literal=adminPassword=<DESIRED_ADMIN_PASSWORD>

```

## Local Development

The makefile can be used to test the helm chart locally by spinning up a Minikube cluster.
This also automates creation of a secret for all the required environment credentials.

```bash
git clone https://gitlab.com/GeorgeRaven/foundryvtt-helm
cd foundryvtt-helm
make FOUNDRY_USERNAME=<YOUR_EMAIL> FOUNDRY_PASSWORD=<YOUR_PASSWORD> ADMIN_PASSWORD=<DESIRED_PASSWORD> all
```
